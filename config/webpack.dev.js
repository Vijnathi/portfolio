var webpackMerged = require('webpack-merge');

var { merge } = require("webpack-merge");
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var ExtractMiniCssPlugin = require('mini-css-extract-plugin');
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');
 
module.exports = merge(commonConfig, {
  devtool: 'cheap-module-eval-source-map',
 
  output: {
    path: helpers.root('dist'),
    publicPath: '/',
    filename: '[name].js',
    chunkFilename: '[id].chunk.js'
  },
 
  plugins: [
    // new ExtractTextPlugin('[name].css'),
    new ExtractMiniCssPlugin({
      filename: `[name].css`
    })
  ],
 
  devServer: {
    historyApiFallback: true,
    stats: 'minimal'
  }
})