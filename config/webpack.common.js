
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var ExtractMiniCssPlugin = require('mini-css-extract-plugin');
var helpers = require('./helpers');
const loader = require('awesome-typescript-loader');
 
module.exports = {
  entry: {
    'polyfills': './src/polyfills.ts',
    'vendor': './src/vendor.ts',
    'app': './src/main.ts',
    'scss': './src/assets/sass/app.scss'
  },
 
  resolve: {
    extensions: ['.scss', '.ts', '.js']
  },
 
  module: {
    rules: [
      {
        test: /\.ts$/,
        loaders: [
          {
            loader: 'awesome-typescript-loader',
            options: { configFileName: helpers.root('src', 'tsconfig.json') }
          } , 'angular2-template-loader'
        ]
      },
      {
        test: /\.html$/,
        loader: 'html-loader'
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        loader: 'file-loader?name=assets/[name].[hash].[ext]'
      },
      // {
      //   test: /\.css$/,
      //   exclude: helpers.root('src', 'app'),
      //   // loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader?sourceMap' })
      //   use: [
      //     "to-string-loader",
      //     // "style-loader",
      //     {
      //       loader: ExtractMiniCssPlugin.loader
      //     },
      //     "css-loader"
      //   ]
      // },
      // {
      //   test: /\.css$/,
      //   include: helpers.root('src', 'app'),
      //   loader: 'raw-loader'
      // }
      { // sass / scss loader for webpack
        test: /\.(sass|scss)$/,
        // loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader']),
        use: [
          // "to-string-loader",
          "style-loader",
          {
            loader: ExtractMiniCssPlugin.loader
          },
          "css-loader",
          "sass-loader"
        ]
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
 
  plugins: [
    // Workaround for angular/angular#11580
    new webpack.ContextReplacementPlugin(
      // The (\\|\/) piece accounts for path separators in *nix and Windows
      /angular(\\|\/)core(\\|\/)@angular/,
      helpers.root('./src'), // location of your src
      {} // a map of your routes
    ),
 
    // new webpack.optimize.CommonsChunkPlugin({
    //   name: ['app', 'vendor', 'polyfills']
    // }),
 
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    })
  ]
};